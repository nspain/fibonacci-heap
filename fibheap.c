/* * * * * * *
 * Author: Nicholas Spain
 * Implementation of a Fibonacci heap as described by (Fredman & Tarjan, 1987).
 *
 * Heap implementation:
 * "Each node contains a pointer to its parent (or to a special node null if it
 * has no parent) and a pointer to one of its children. The children of each
 * node are doubly linked in a circular list. Each node also contains its rank
 * and a bit indicating whether it is marked. The roots of all the trees in the
 * heap are doubly linked in a circular list. We accessthe heap by a pointer to
 * a root containing an item of minimum key; we call this root the minimum node
 * of the heap. A minimum node of null denotes an empty heap."
 * (Fredman & Tarjan, 1987)
 * * * * * * */

#include "fibheap.h"
#include "clist.h"
#include "hashmap.h"
#include "utils.h"
#include <stdlib.h>

// Node in the tree used as the Fibonacci heap
struct Node {
    void *data;
    struct Clist *kids;
};

struct Fheap *fheap_make(void *array, size_t n, size_t width) {
    struct Fheap *fheap;
    fheap = safe_malloc(sizeof(*fheap));
    fheap->n = n;
    fheap->width = width;
    fheap->size = n * width;
    fheap->heap = safe_malloc(fheap->size);
    return fheap;
}

// Worst-case: O(1)
void fheap_insert(void *i, struct Fheap *fheap) { return; }

// Worst-case: O(1)
void *fheap_peekmin(struct Fheap *fheap) { return fheap->heap[0]; }

// Worst-case: O(log(n))
void fheap_deletemin(struct Fheap *fheap) { return; }

struct Fheap *fheap_merge(struct Fheap *fheap1, struct Fheap *fheap2) {
    struct Fheap *merged;
    return merged;
}

// Worst-case: O(1)
void fheap_decreasekey(int delta, void *i, struct Fheap *fheap) { return; }

// Worst-case: O(log(n))
void fheap_delete(void *i, struct Fheap *fheap) { return; }
