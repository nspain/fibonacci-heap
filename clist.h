/* * * * * * *
 * Author: Nicholas Spain
 *
 * Library for the creation and manipulation of circular doubly linked lists.
 * * * * * * */

#ifndef CLIST_H

#define CLIST_H

#include <stdlib.h>

struct Clist;

struct Clist *clist_make();

void clist_add_head(void *i, struct Clist *clist);

void clist_add_tail(void *i, struct Clist *clist);

void *clist_remove_head(struct Clist *clist);

void *clist_remove_tail(struct Clist *clist);

void clist_free(struct Clist *clist);

#endif
