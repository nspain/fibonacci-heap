/* * * * * * *
 * Author: Nicholas Spain
 *
 * File for testing out the Fibonacci heap implemented in fibheap.c
 * * * * * * */

#include <stdio.h>
#include <stdlib.h>
#include "fibheap.h"

int main() {
    struct Fheap *fheap;
    int array[] = {2, 3, 6, 7, 8, 11, 16, 9, 12, 2, 6, 8};
    fheap = fheap_make(array, 12, sizeof(*array));
    printf("Printing heap in heap ordering...\n");
    for (size_t i = 0; i < fheap->n; i++) {
        printf("fheap->heap[%lu] = %d\n", i, (int)fheap->heap[i]);
    }
    return 0;
}
