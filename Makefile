CFLAGS = -Wall -Wextra -Wuninitialized
EXE =  heap-test
OBJS = fibheap.o utils.o main.o clist.o

$(EXE): $(OBJS)
	$(CC) $(CFLAGS) -o $(EXE) $(OBJS)

fibheap.o: fibheap.h utils.h clist.o
utils.o: utils.h
clist.o: clist.h
main.o: fibheap.h

clean:
	rm -f $(OBJS) $(EXE)
