/* * * * * * *
 * Author: Nicholas Spain
 * Implementation of a Fibonacci heap as described by (Fredman & Tarjan, 1987).
 * * * * * * */

#ifndef FIBHEAP_H

#define FIBHEAP_H

#include "clist.h"
#include "hashmap.h"
#include <stdlib.h>

/* Fibonacci heap data structured. Used to make it easier to pass the heap
 * around. Contains the heap structure HEAP, represented as a partially ordered
 * array, the number of elements in the heap N, size of the heap in bytes,
 * a map MAP allowing constant time access to all elements in the heap, and
 * an integer array KEY containing the keys, priorities, for each element. */
struct Fheap {
    void **heap;
    size_t n, size, width;
    struct Hashmap *map;
    int *key;
};

/* Create a heap from ARRAY and put it into FHEAP. The size of each element in
 * ARRAY that is to be inserted in FHEAP is WIDTH bytes. */
struct Fheap *fheap_make(void *array, size_t n, size_t width);

/* Insert element I in FHEAP. */
void fheap_insert(void *i, struct Fheap *fheap);

/* Peek at the minimum in FHEAP. */
void *fheap_peekmin(struct Fheap *fheap);

/* Delete the minimum in FHEAP. */
void fheap_deletemin(struct Fheap *fheap);

/* Merge two heaps together an return a new heap. */
struct Fheap *fheap_merge(struct Fheap *fheap1, struct Fheap *fheap2);

/* Decrease the key of element I in FHEAP by DELTA. */
void fheap_decreasekey(int delta, void *i, struct Fheap *fheap);

/* Delete element I from FHEAP. */
void fheap_delete(void *i, struct Fheap *fheap);

#endif
