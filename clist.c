/* * * * * * *
 * Author: Nicholas Spain
 *
 * Library for the creation and manipulation of circular doubly linked lists.
 * * * * * * */

#include "clist.h"
#include "utils.h"
#include <assert.h>
#include <stdlib.h>

struct Node {
    void *data;
    struct Node *next, *prev;
};

struct Clist {
    struct Node *head, *tail;
};

struct Clist *clist_make() {
    struct Clist *clist;
    clist = safe_malloc(sizeof(*clist));
    clist->head = safe_malloc(sizeof(*clist->head));
    clist->tail = safe_malloc(sizeof(*clist->tail));

    // Initialise head to null so we know the list is empty
    clist->head = NULL;
    clist->tail = NULL;
    return clist;
}

void clist_add_head(void *i, struct Clist *clist) {
    assert(clist);
    struct Node *new;
    new = safe_malloc(sizeof(*new));
    new->data = i;
    new->next = clist->head;
    clist->head = new;
    if (clist->tail == NULL) {
        clist->tail = new;
    }
    clist->tail->next = clist->head; // Make circular
}

void clist_add_tail(void *i, struct Clist *clist) {
    assert(clist);
    struct Node *new;
    new = safe_malloc(sizeof(*new));
    new->data = i;
    new->next = clist->head;   // Make circular;
    if (clist->tail == NULL) { // First insertion
        clist->head = clist->tail = new;
    } else {
        clist->tail->next = new;
        clist->tail = new;
    }
}

void *clist_remove_head(struct Clist *clist) {
    assert(clist);
    void *data;
    struct Node *old;
    old = clist->head;
    data = old->data;
    clist->head = clist->head->next;
    clist->tail->next = clist->head; // Make circular
    free(old);
    return data;
}

void *clist_remove_tail(struct Clist *clist) {
    assert(clist);
    void *data;
    struct Node *old;
    old = clist->tail;
    data = old->data;
    clist->tail = clist->head->prev;
    clist->tail->next = clist->head; // Make circular
    free(old);
    return data;
}

void clist_free(struct Clist *clist) {
    assert(clist);
    struct Node *curr, *prev;
    curr = clist->head;
    while (curr) {
        prev = curr;
        curr = curr->next;
        free(prev);
        prev = NULL; // Set to null so we stop on the next round
    }
    free(clist);
}
